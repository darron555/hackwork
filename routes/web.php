<?php

use Illuminate\Http\Response;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main');
});
//
//Route::post('/authenticate', 'Auth\LoginController@authenticate');
//Route::get('/get', 'Auth\LoginController@index')->middleware('api.auth');
//Route::get('/token', 'Auth\LoginController@getToken');
//Route::get('/delete', 'Auth\LoginController@deleteToken');
//
//Route::get('/employer/user/info', function () {
//    return view('employment_user_info');
//})->middleware('auth');
//
//Route::get('/jobseeker/user/info', function () {
//    return view('jobseeker_user_info');
//})->middleware('auth');
//
//Route::get('/employer/job', function () {
//    return view('job_adding');
//})->middleware('auth');
//
//Route::get('/jobseeker/job', function () {
//    return view('job_looking');
//})->middleware('auth');
//
//Route::get('/jobseeker/list', function () {
//    return view('job_list');
//})->middleware('auth');
//
//Auth::routes();
//
//Route::get('/home', 'HomeController@index');
