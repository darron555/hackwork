function controlHeight() {
    if ($(window).width() < 768) return;
    var delta = $(window).height() - $(".top_block").outerHeight();
    $('.content_height_control_js').css('min-height', delta);
};
function controlWidth() {
    if (($('.content_wrapper').length > 0) && ($(window).width() > 940)) {
        var left_side_width = $('.left_panel').innerWidth();
        var full_width = $('.content_wrapper').innerWidth();
        var right_side_width = full_width - left_side_width;
        $('.content_panel').innerWidth(right_side_width);
        $('.content_panel').css('margin-left', left_side_width);

        $('.top_left_width_js').innerWidth(left_side_width);
        $('.top_content_width_js').innerWidth(right_side_width);
    }
};


// A $( document ).ready() block.
$(document).ready(function () {

    controlHeight();
    controlWidth();
    setTimeout(function () {
        controlWidth();
    }, 100)
    $(window).resize(function () {
        controlHeight();
        controlWidth();
    });

    if (($('#mm-menu').length > 0) && ($(window).width() < 940)) {
        $('#mm-menu').addClass('invisible').addClass('mm-menu');
        $('.mm-menu__items li').each(function (indx, element) {
            $(element).addClass('mm-menu__item');
        });
        var menu = new Menu;
        setTimeout(function () {
            $('#mm-menu').removeClass('invisible');
        }, 500)

    }

    jQuery(function ($) {
        $("input[name='phone']").mask("+375(99) 999-99-99");
        $("input[name='time']").mask("99-99");
    });

    if ($('input.i_check').length > 0) {
        $('input.i_check').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });
    }
    if ($('.selectpicker').length > 0) {
        $('.selectpicker').selectpicker();
    }


    $('.rating_list:not(.locked)>li').hover(
        function () {
            var rating = $(this).data('rating');
            for (var n = 1; n <= rating; n++) {
                $(this).parent().find('li:nth-child(' + n + ')').addClass('hover');
            }
        },
        function () {
            $('.rating_list>li').removeClass('hover');
        });


    $(".rating_list:not(.locked)>li").click(function (e) {
        e.preventDefault();
        $(this).parent().children('li').removeClass('active');
        var rating = $(this).data('rating');
        for (var n = 1; n <= rating; n++) {
            $(this).parent().find('li:nth-child(' + n + ')').addClass('active');
        }
    });


    // Dima
    $('#register').on('click', function (e) {
        e.preventDefault();
        var data = [];
        // var data = $(this).serializeArray();
        $('#modalRegister input').each(function (i, elem) {
            data[$(this).attr('name')] = $(this).val();

        });
        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': data['_token']}
        });

        $.ajax({
            type: 'POST',
            url: 'api/register',
            data: {'name': data['name'], 'email': data['email'], 'city': data['city'], 'password': data['password']},
            success: function (res) {
                console.log(res);
            },
            error: function (res) {
                $('#modalRegister').append('<div style="margin-top: 10px" class="alert alert-danger" role="alert">Произошла ошибка! Проверьте вводимые данные!</div>');

            }
        });
    });


    $('#login').on('click', function (e) {
        e.preventDefault();
        var data = [];

        $('#modalLogin input').each(function (i, elem) {
            data[$(this).attr('name')] = $(this).val();

        });
        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': data['_token']}
        });

        $.ajax({
            type: 'POST',
            url: 'api/login',
            data: {'email': data['email'], 'password': data['password']},
            success: function (res) {
                console.log(res);
                $.ajaxSetup({
                    headers: {Authorization: 'Bearer '+ res.token}
                });
                $.ajax({
                    type: 'GET',
                    url: 'api/test',
                    success: function (result) {
                         console.log(result);

                    }

                });
            },
            error: function (res) {
                console.log(res);
                $.ajaxSetup({
                    headers: {Authorization: res.token_type +'dgdfgdfg'}
                });
                $.ajax({
                    type: 'GET',
                    url: '/get',
                    success: function (result) {
                        console.log(result);

                    }

                });
            }
        });
    });

    $('#inputPasswordRepeat').on('keyup', function (e) {
        $('[role="alert"]').remove();
        var text = $('#inputPassword').val();
        $(this).val();
        if ($(this).val() != text) {
            $(this).parent('div').append('<div style="margin-top: 10px" class="alert alert-danger" role="alert">Пароли не совпадают</div>');
            $('#register').addClass('disabled');
        } else {
            $('#register').removeClass('disabled');
        }

    });

    $('.no-enter').on('click', function (e) {
        e.preventDefault();
        alert('Зарегистрируйтесь или войдите в систему');
    });


    //$( window ).load(function() {

    //});

});