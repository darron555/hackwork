
@extends('layouts.user_layout')

@section('content')
<body class="performer_theme">

<div id="wrapper" class="wrapper">
    <div class="container-fluid">

        @include('seeker_top')

        <div class="row content_wrapper">
            <div class="content_panel content_height_control_js">
                <ul class="list-inline socials">
                    <li>
                        <a href="#" class="fa-stack fa-lg">
                            <i class="fa fa-circle-thin fa-stack-2x"></i>
                            <i class="fa fa-facebook fa-stack-1x"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="fa-stack fa-lg">
                            <i class="fa fa-circle-thin fa-stack-2x"></i>
                            <i class="fa fa-twitter fa-stack-1x"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="fa-stack fa-lg">
                            <i class="fa fa-circle-thin fa-stack-2x"></i>
                            <i class="fa fa-vk fa-stack-1x"></i>
                        </a>
                    </li>
                </ul>

                <div class="col-sm-8 col-sm-offset-2 col-md-12 col-md-offset-0 col-lg-10 col-lg-offset-1">
                    <div class="personal_information_form">
                        <div class="form_title h3">Характеристика</div>
                        <div class="form-group">
                            <label>Специализация:</label>
                            <a href="#" class="specialization_link" data-toggle="modal" data-target="#specialization_modal"><span class="glyphicon glyphicon-plus"></span> Выберите специализацию</a>

                            <!-- Modal -->
                            <div class="modal fade" id="specialization_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-md">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <div class="modal-title"><span class="ff_pbc logo_txt">HACKWORK</span></div>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <a href="#" class="btn btn-warning btn-block h20px">Выбрать</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="exampleInput1">Опыт работы:</label>
                            <select class="selectpicker form-control" id="exampleInput1">
                                <option>12 лет</option>
                                <option>14 лет</option>
                                <option>16 лет</option>
                                <option>18 лет</option>
                                <option>20 лет</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInput2">Укажите стоимость месяца вашей работы (в белорусских рублях):</label>
                            <select class="selectpicker form-control" id="exampleInput2">
                                <option>10 000 000</option>
                                <option>20 000 000</option>
                                <option>30 000 000</option>
                                <option>40 000 000</option>
                                <option>50 000 000</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInput3">Укажите стоимость часа вашей работы (в белорусских рублях):</label>
                            <select class="selectpicker form-control" id="exampleInput3">
                                <option>400 000</option>
                                <option>500 000</option>
                                <option>600 000</option>
                                <option>700 000</option>
                                <option>800 000</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInput4">Умения и сертификаты:</label>
                            <select class="selectpicker form-control" multiple id="exampleInput4">
                                <option selected>Adobe Photoshop</option>
                                <option selected>Adobe Illustrator</option>
                                <option selected>Axure</option>
                                <option>Avacode</option>
                                <option>Acorel</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleDescription">О себе:</label>
                            <textarea class="form-control" placeholder="Расскажите о себе пользователям" rows="5" id="exampleDescription"></textarea>
                        </div>
                        <button type="submit" class="btn btn-warning fs20 mt25">Сохранить данные</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<button id="mm-menu-toggle" class="mm-menu-toggle">Toggle Menu</button>
<nav id="mm-menu" class="">
    <div class="arrow_substrate"></div>
    <div class="left_panel">
        <div class="image_container">
            <div class="profile_image_circle">
                <img src="img/jobseeker-icon.png" class="img-responsive">
            </div>
            <div class="profile_use_name ">Иван
                Иванович </div>
        </div>
        <div class="left_menu_block">
            <div class="menu_title text-uppercase">
                <i class="sp_icon icon_cog dark"></i>
                Личный кабинет
            </div>
            <ul class="list-group mm-menu__items">
                <li class="">
                    <a href="/jobseeker/user/info" class="list-group-item">Персональные данные</a>
                </li>
                <li class="">
                    <a href="#" class="list-group-item active">Характеристика</a>
                </li>
                <li class="">
                    <a href="#" class="list-group-item">Отзывы</a>
                </li>
                <li class="">
                    <a href="#" class="list-group-item">Уведомления</a>
                </li>
                <li class="">
                    <a href="#" class="list-group-item">Личные сообщения</a>
                </li>
                <li class="">
                    <a href="#" class="list-group-item">Смена пароля</a>
                </li>
            </ul>
        </div>
        <a href="#" class="hackwork_link">hackwork.com</a>
    </div>
</nav><!-- /nav -->



</body>
@endsection