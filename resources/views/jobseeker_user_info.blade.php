@extends('layouts.user_layout')

@section('content')
<body class="performer_theme">

<div id="wrapper" class="wrapper">
    <div class="container-fluid">

            @include('seeker_top')

        <div class="row content_wrapper">
            <div class="content_panel content_height_control_js">
                <ul class="list-inline socials">
                    <li>
                        <a href="#" class="fa-stack fa-lg">
                            <i class="fa fa-circle-thin fa-stack-2x"></i>
                            <i class="fa fa-facebook fa-stack-1x"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="fa-stack fa-lg">
                            <i class="fa fa-circle-thin fa-stack-2x"></i>
                            <i class="fa fa-twitter fa-stack-1x"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="fa-stack fa-lg">
                            <i class="fa fa-circle-thin fa-stack-2x"></i>
                            <i class="fa fa-vk fa-stack-1x"></i>
                        </a>
                    </li>
                </ul>

                <div class="col-sm-8 col-sm-offset-2 col-md-12 col-md-offset-0 col-lg-10 col-lg-offset-1">
                    <div class="personal_information_form">
                        <div class="form_title h3">Персональные данные:</div>
                        <div class="form-group">
                            <label for="exampleInput1">ФИО:</label>
                            <input type="text" class="form-control" value="{{ Auth::user()->name }}" placeholder="Иванов Иван Иванович" id="exampleInput1">
                        </div>
                        <div class="form-group">
                            <label for="exampleInput2">Ваш город:</label>
                            <input type="text" class="form-control" value="{{ Auth::user()->city }}"  placeholder="Минск" id="exampleInput2">
                        </div>
                        <div class="form-group">
                            <label for="exampleInput3">Электронный адрес:</label>
                            <input type="text" class="form-control" value="{{ Auth::user()->email }}"  placeholder="ivan@gmail.com" id="exampleInput3">
                        </div>
                        <div class="form-group">
                            <label for="exampleInput4">Телефон:</label>
                            <input type="text" class="form-control" name="phone" placeholder="+375(__) ___-__-__" id="exampleInput4">
                        </div>
                        <div class="form-group">
                            <label for="exampleInput5">Skype:</label>
                            <input type="text" class="form-control" placeholder="ivan_ivan" id="exampleInput5">
                        </div>
                        <div class="form-group">
                            <label>Удобное время для связи:</label>
                            <div class="time_group">
                                <label for="exampleInput6-1">c</label>
                                <input type="text" class="form-control"  name="time" placeholder="09-00" id="exampleInput6-1">
                                <label for="exampleInput6-2">по</label>
                                <input type="text" class="form-control"  name="time" placeholder="18-00" id="exampleInput6-2">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-warning fs20 mt25">Сохранить данные</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<button id="mm-menu-toggle" class="mm-menu-toggle">Toggle Menu</button>
<nav id="mm-menu" class="">
    <div class="arrow_substrate"></div>
    <div class="left_panel">
        <div class="image_container">
            <div class="profile_image_circle">
                <img src="{{asset('img/jobseeker-icon.png')}}" class="img-responsive">
            </div>
            <div class="profile_use_name ">{{ Auth::user()->name }}</div>
        </div>
        <div class="left_menu_block">
            <div class="menu_title text-uppercase">
                <i class="sp_icon icon_cog dark"></i>
                Личный кабинет
            </div>
            <ul class="list-group mm-menu__items">
                <li class="">
                    <a href="/jobseeker/user/info" class="list-group-item active">Персональные данные</a>
                </li>
                <li class="">
                    <a href="#" class="list-group-item">Характеристика</a>
                </li>
                <li class="">
                    <a href="#" class="list-group-item">Отзывы</a>
                </li>
                <li class="">
                    <a href="#" class="list-group-item">Уведомления</a>
                </li>
                <li class="">
                    <a href="#" class="list-group-item">Личные сообщения</a>
                </li>
                <li class="">
                    <a href="#" class="list-group-item">Смена пароля</a>
                </li>
            </ul>
        </div>
        <a href="#" class="hackwork_link">hackwork.com</a>
    </div>
</nav><!-- /nav -->


</body>
@endsection