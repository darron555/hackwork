
@extends('layouts.user_layout')

@section('content')

<body class="employer_theme">

<div id="wrapper" class="wrapper">
    <div class="container-fluid">
        @include('emloyer_top')
        <div class="row content_wrapper">
            <div class="content_panel content_height_control_js">
                <ul class="list-inline socials">
                    <li>
                        <a href="#" class="fa-stack fa-lg">
                            <i class="fa fa-circle-thin fa-stack-2x"></i>
                            <i class="fa fa-facebook fa-stack-1x"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="fa-stack fa-lg">
                            <i class="fa fa-circle-thin fa-stack-2x"></i>
                            <i class="fa fa-twitter fa-stack-1x"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="fa-stack fa-lg">
                            <i class="fa fa-circle-thin fa-stack-2x"></i>
                            <i class="fa fa-vk fa-stack-1x"></i>
                        </a>
                    </li>
                </ul>

                <div class="col-sm-8 col-sm-offset-2 col-md-12 col-md-offset-0 col-lg-10 col-lg-offset-1">
                    <div class="personal_information_form">
                        <div class="form_title h3">Добавление вакансии</div>
                        <div class="attention_block mb15">
                            <i class="attention-icon"></i> <span class="text-danger">Все поля обязательны для заполнения</span>
                        </div>
                        <div class="form-group">
                            <label for="exampleTitle1">Заголовок:</label>
                            <input type="text" class="form-control" placeholder="Нужен адвокат по гражданскому несложному делу" id="exampleTitle1">
                        </div>
                        <div class="form-group">
                            <label for="exampleSpecialization2">Специализация:</label>
                            <input type="text" class="form-control"  placeholder="Адвокат" id="exampleSpecialization2">
                        </div>
                        <div class="form-group">
                            <label>Вознаграждение (в белорусских рублях):</label>
                            <div class="time_group">
                                <label for="exampleInput6-1">от</label>
                                <input type="text" class="form-control"  placeholder="1 000 000" id="exampleInput6-1">
                                <label for="exampleInput6-2">до</label>
                                <input type="text" class="form-control"  placeholder="" id="exampleInput6-2">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInput4">Местоположение:</label>
                            <input type="text" class="form-control" placeholder="Введите название населённого пункта" id="exampleInput4">
                        </div>
                        <div class="form-group">
                            <label for="exampleDemands5">Требования к исполнителю:</label>
                            <input type="text" class="form-control" placeholder="Опыт ведения граждаских дел" id="exampleDemands5">
                        </div>
                        <div class="form-group">
                            <label for="exampleDescription1">Подробное описание:</label>
                            <textarea class="form-control" placeholder="Иванов Иван Иванович" rows="5" id="exampleDescription1"></textarea>
                        </div>
                        <button type="submit" class="btn btn-warning fs20 mt25">Сохранить данные</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- /wrapper -->

<button id="mm-menu-toggle" class="mm-menu-toggle">Toggle Menu</button>
<nav id="mm-menu" class="">
    <div class="arrow_substrate"></div>
    <div class="left_panel">
        <div class="image_container">
            <div class="profile_image_circle">
                <img src="img/ivan-icon.png" class="img-responsive">
            </div>
            <div class="profile_use_name ">{{ Auth::user()->name }}</div>
        </div>
        <div class="left_menu_block">
            <div class="menu_title text-uppercase">
                <i class="sp_icon icon_cog dark"></i>
                Личный кабинет
            </div>
            <ul class="list-group mm-menu__items">
                <li class="">
                    <a href="/employer/user/info" class="list-group-item active">Персональные данные</a>
                </li>
                <li class="">
                    <a href="#" class="list-group-item">Характеристика</a>
                </li>
                <li class="">
                    <a href="#" class="list-group-item">Отзывы</a>
                </li>
                <li class="">
                    <a href="#" class="list-group-item">Уведомления</a>
                </li>
                <li class="">
                    <a href="#" class="list-group-item">Личные сообщения</a>
                </li>
                <li class="">
                    <a href="#" class="list-group-item">Смена пароля</a>
                </li>
            </ul>
        </div>
        <a href="#" class="hackwork_link">hackwork.com</a>
    </div>
</nav><!-- /nav -->


</body>
@endsection
{{--
</html>--}}
