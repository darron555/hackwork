@extends('layouts.layout')

@section('content')

    <div class="s_wrapper">
        <div class="s_wrapper_bg">
            <div class="s_wrapper_inner">
                <div class="header_block">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="left_part text-left">
                                <ul class="list-inline socials">
                                    <li>
                                        <a href="#" class="fa-stack fa-lg">
                                            <i class="fa fa-circle-thin fa-stack-2x"></i>
                                            <i class="fa fa-facebook fa-stack-1x"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="fa-stack fa-lg">
                                            <i class="fa fa-circle-thin fa-stack-2x"></i>
                                            <i class="fa fa-twitter fa-stack-1x"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="fa-stack fa-lg">
                                            <i class="fa fa-circle-thin fa-stack-2x"></i>
                                            <i class="fa fa-vk fa-stack-1x"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="middle_part text-center">
                                <a href="#" class="ff_pbc logo_txt">HackWork</a>
                            </div>
                        </div>
                        @if (Auth::guest())
                        <div class="col-sm-4">
                            <div class="right_part text-right">
                                <ul class="list-inline authorizations_options">
                                    <li>
                                        <a href="#" data-toggle="modal" data-target="#registration_modal">Регистрация</a>
                                    </li>
                                    <li>
                                        <a href="#" data-toggle="modal" data-target="#authorization_modal">Вход</a>
                                    </li>
                                    <li>
                                        <a href="#">Где я?</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        @else
                            <div class="right">Добро пожаловать,  {{ Auth::user()->name }} </div>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                Выход
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>

                        @endif

                    </div>
                </div>
                <div class="working_block">
                    <div class="row">
                        <div class="col-sm-6 pos_rel">
                            <div class="text-center left_part max_width">
                                <div class="icon">
                                    <img src="img/employer-main-icon.png" class="img-responsive"/>
                                </div>
                                <div class="text light">
                                    Герой трагедии «Фауст» - личность
                                    историческая, он жил в XVI веке, слыл
                                    магом и чернокнижником и, отвергнув
                                    современную науку и религию, продал
                                    душу дьяволу.
                                </div>
                                <div class="button">
                                    @if (Auth::guest())
                                        <a href="#" class="large_btn white no-enter">Разместить работу</a>
                                    @else
                                        <a href="/employer/job" class="large_btn white">Разместить работу</a>

                                    @endif

                                    <div class="line_half"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 pos_rel">
                            <div class="text-center right_part max_width">
                                <div class="icon">
                                    <img src="img/jobseeker-main-icon.png" class="img-responsive"/>
                                </div>
                                <div class="text light">
                                    Свежих людей редко видят в палате
                                    N6. Новых помешанных доктор давно
                                    не принимает, а любителей посещать
                                    сумасшедшие дома уже немного на
                                    этом свете.
                                </div>
                                <div class="button">
                                    @if (Auth::guest())
                                        <a href="#" class="large_btn white no-enter">Найти работу</a>
                                    @else
                                        <a href="/jobseeker/list" class="large_btn white">Найти работу</a>
                                    @endif

                                    <div class="line_half"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modals">
        <!-- Modal -->
        <div class="modal fade" id="registration_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <div class="modal-title"><span class="ff_pbc logo_txt">HACKWORK</span></div>
                    </div>
                    <div class="modal-body" id="modalRegister">

                            <form class="form-horizontal" role="form" method="POST" action="">
                                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="name" id="inputName" placeholder="Имя и Фамилия"/>
                                </div>

                                <div class="form-group">
                                    <input type="text" class="form-control" name="city" id="inputCity" placeholder="Ваш город"/>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="email" id="inputEmail" placeholder="Электронный адрес"/>
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" name="password" id="inputPassword" placeholder="Пароль"/>
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" name="password_confirm" id="inputPasswordRepeat" placeholder="Подтвердите пароль"/>
                                </div>
                                <div class="form-group">
                                    <button  id="register" class="btn btn-warning btn-block h20px">Зарегистрироваться</button>
                                </div>
                                <div class="license_agreement">
                                    При нажатии “Зарегистрироваться” Вы соглашаетесь с
                                    <a href="#">Политикой конфиденциальности</a> и <a href="#">Условиями пользования</a>
                                </div>
                            </form>

                    </div>

                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="authorization_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md">
                <div class="modal-content" id="modalLogin">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <div class="modal-title"><span class="ff_pbc logo_txt">HACKWORK</span></div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="text" class="form-control" name="email" id="inputEmail2" placeholder="Электронный адрес"/>
                        </div>
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" id="inputPassword2" placeholder="Пароль"/>
                        </div>
                        <div class="form-group">
                            <button  id="login" class="btn btn-warning btn-block h20px">Войти</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection