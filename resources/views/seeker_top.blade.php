<div class="top_block">
<div class="row top_line">
        <div class="col-sm-3 col-lg-2 top_left_width_js">
            <a href="/" class="ff_pbc top_line_logo">HackWork</a >
        </div>
        <div class="col-sm-9 col-lg-10 top_content_width_js">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-2 col-md-12 col-md-offset-0 col-lg-11 col-lg-offset-1">
                    <ul class="list-inline main_ul text-left">
                        <li>
                            <a href="#">
                                <span>Исполнители</span>
                            </a>
                        </li>
                        <li>
                            <a href="/jobseeker/list">
                                <span>Работа</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>Помощь и поддержка</span>
                            </a>
                        </li>
                    </ul>
                    <ul class="list-inline login_ul text-right">
                        <li>
                            <a href="/jobseeker/user/info" class="email_link">
                                <span>{{ Auth::user()->email }}</span>
                            </a>
                        </li>
                        <li>
                            <a href="/employer/user/info">
                                <i class="performer_icon"></i>
                                <span>Войти работодателем</span>
                            </a>
                        </li>
                        <li>
                            <a href="/" onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                                <i class="lock_icon"></i>
                                <span>Выйти</span>
                            </a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>