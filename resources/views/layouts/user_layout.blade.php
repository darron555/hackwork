<!DOCTYPE html>
<html>
<head lang="en">
    <title>Account</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->

    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300,500&subset=latin,cyrillic' rel='stylesheet' type='text/css'>

    <!-- Bootstrap -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link href="icheck-1.x/skins/minimal/blue.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/materialMenu.css')}}">
    <link href="{{asset('css/bootstrap-select.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.maskedinput.js')}}" type="text/javascript"></script>
    <script src="{{asset('icheck-1.x/icheck.js')}}"></script>
    <script src="{{asset('js/materialMenu.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-select.js')}}"></script>
    <!-- Include  user-js -->
    <script src="{{asset('js/script.js')}}"></script>
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>



@yield('content')


</html>