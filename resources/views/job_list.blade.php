@extends('layouts.user_layout')

@section('content')
<body class="performer_theme">

<div id="wrapper" class="wrapper">
    <div class="container-fluid">
        @include('seeker_top')
        <div class="row content_wrapper">
            <div class="content_panel content_height_control_js">
                <ul class="list-inline socials">
                    <li>
                        <a href="#" class="fa-stack fa-lg">
                            <i class="fa fa-circle-thin fa-stack-2x"></i>
                            <i class="fa fa-facebook fa-stack-1x"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="fa-stack fa-lg">
                            <i class="fa fa-circle-thin fa-stack-2x"></i>
                            <i class="fa fa-twitter fa-stack-1x"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="fa-stack fa-lg">
                            <i class="fa fa-circle-thin fa-stack-2x"></i>
                            <i class="fa fa-vk fa-stack-1x"></i>
                        </a>
                    </li>
                </ul>

                <div class="col-sm-8 col-sm-offset-2 col-md-12 col-md-offset-0 col-lg-10 col-lg-offset-1">
                    <div class="personal_information_form">
                        <div class="form_title h3">Всего в базе 5434 вакансий</div>
                        <div class="search_group">
                            <span class="glyphicon glyphicon-search"></span>
                            <select class="selectpicker form-control" data-live-search="true">
                                <option selected disabled>Поиск по имени, специализации</option>
                                <option>Adobe Illustrator</option>
                                <option>Axure</option>
                                <option>Avacode</option>
                                <option>Acorel</option>
                            </select>
                        </div>
                        <div class="performer_preview_item">
                            <div class="row">
                                <div class="col-xs-12 col-md-3 col-lg-2">
                                    <div class="left_part">
                                        <a href="#"><img src="{{asset('img/work1.png')}}" class="img-circle img-responsive avatar_img" /></a>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-9 col-lg-10">
                                    <div class="right_part">
                                        <div class="title">
                                            <a href="#" class="name fs24">Разработать слайдер из 4 слайдов</a>
                                            <span class="label label-default">от 1000 $</span>
                                        </div>
                                        <ul class="list-inline attributes">
                                            <li class="company">Ai print house</li>
                                            <li>Минск</li>
                                            <li>50 минут назад</li>
                                        </ul>
                                        <div class="text">
                                            Уважаемые Исполнители! Нужно разработать слайдер из 4 слайдов, где должны быть визуализированы
                                            следующие пункты УТП (уникальное торговое предложение) и Специальные Акции
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="performer_preview_item">
                            <div class="row">
                                <div class="col-xs-12 col-md-3 col-lg-2">
                                    <div class="left_part">
                                        <a href="#"><img src="{{asset('img/work2.png')}}" class="img-circle img-responsive avatar_img" /></a>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-9 col-lg-10">
                                    <div class="right_part">
                                        <div class="title">
                                            <a href="#" class="name fs24">Редизайн в muse </a>
                                            <span class="label label-default">от 300 $</span>
                                        </div>
                                        <ul class="list-inline attributes">
                                            <li class="company">Perpetuum mobile</li>
                                            <li>Гомель</li>
                                            <li> час 21 минуту назад</li>
                                        </ul>
                                        <div class="text">
                                            Требуется сделать редизайн лендинга видеоперец.рф сверстан в muse Ищу дизайнера со вкусом.<br>
                                            Желательно сделать в стиле инфографики.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="performer_preview_item">
                            <div class="row">
                                <div class="col-xs-12 col-md-3 col-lg-2">
                                    <div class="left_part">
                                        <a href="#"><img src="{{asset('img/work3.png')}}" class="img-circle img-responsive avatar_img" /></a>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-9 col-lg-10">
                                    <div class="right_part">
                                        <div class="title">
                                            <a href="#" class="name fs24">Продающий дизайн сайта Landing Page</a>
                                            <span class="label label-default">от 230 $</span>
                                        </div>
                                        <ul class="list-inline attributes">
                                            <li class="company">GraphicsInc</li>
                                            <li>Полоцк</li>
                                            <li>2 часа назад</li>
                                        </ul>
                                        <div class="text">
                                            Всем привет ребята. Нужен продающий дизайн сайта Landing Page, 10 блоков, стандартная структура.<br>
                                            Производственная тематика, производство кабеля.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<button id="mm-menu-toggle" class="mm-menu-toggle">Toggle Menu</button>
<nav id="mm-menu" class="">
    <div class="arrow_substrate"></div>>
    <div class="left_panel">
        <div class="left_form">
            <div class="form-group">
                <label>Специализация</label>
                <a href="#" class="specialization_link" data-toggle="modal" data-target="#specialization_modal"><span class="glyphicon glyphicon-plus"></span> Выберите специализацию</a>
                <!-- Modal -->
                <div class="modal fade" id="specialization_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-md">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <div class="modal-title"><span class="ff_pbc logo_txt">HACKWORK</span></div>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <a href="#" class="btn btn-warning btn-block h20px">Выбрать</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="exampleInput50">Местоположение</label>
                <input type="text" class="form-control" placeholder="Минск" id="exampleInput50">
            </div>
            <div class="form-group">
                <label for="exampleInput60">Стоимость работ от </label>
                <div class="with_on">
                    <input type="text" class="form-control" placeholder="1 000 000" id="exampleInput60">
                    <span class="on_input">бел. руб.</span>
                </div>
            </div>
            <div class="form-group">
                <label>Дополнительно</label>
                <div class="checkbox w100per">
                    <label>
                        <input class="i_check" type="checkbox" checked> С отывами о работодателе
                    </label>
                </div>
                <div class="clearfix"></div>
            </div>
            <button type="submit" class="btn btn-warning fs20 mt25">Подобрать предложения</button>
        </div>
        <a href="#" class="hackwork_link">hackwork.com</a>
    </div>
</nav><!-- /nav -->

</body>
@endsection