<?php


namespace app\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User;

class AuthController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function refreshToken()
    {
        $token = JWTAuth::getToken();

        if(!$token) {
            return response()->json(['error' => 'неправильный токен']);
        }

        try {
            $refreshToken = JWTAuth::refresh($token);
        } catch (JWTException $exception) {
            return response()->json(['error' => 'ошибка']);
        }

        return response()->json(compact('token'));
    }

    public function deleteToken()
    {
        $user = JWTAuth::parseToken()->authenticate();

        if(!$user){
            // нет юзера
        }

        $user->delete();

    }


    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->error(['error' => 'Email или пароль введены неверно!'], 401);
            }
        } catch (JWTException $exception) {
            return response()->errorInternal();
        }

        return response()->json(compact('token'));
    }

    public function register(Request $request)
    {
        $newUser = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'city' => $request->get('city'),
            'password' => bcrypt($request->get('password')),
        ];
        $user = User::create($newUser);
        $token = JWTAuth::fromUser($user);

        return response()->json(compact('token'));
    }

}