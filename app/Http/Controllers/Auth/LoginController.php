<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

//    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->error(['error' => 'Email или пароль введены неверно!'], 401);
            }
        } catch (JWTException $exception) {
            return response()->errorInternal();
        }

        return response([
            'token_type' => 'Bearer',
            'access_token' => $token,
        ]);
    }

    public function getAuthUser()
    {
        try {
            if (!$user = JWTAuth::parseToken()->toUser()) {
                return response()->error(['error' => 'Пользователь не найден'], 404);
            }
        } catch (JWTException $exception) {
            return response()->errorInternal();
        }

        return response()->item($user, new UserTransformer)->setStatusCode(200);
    }

    public function getToken()
    {
        $token = JWTAuth::getToken();

        if(!$token) {
            return response()->json(['error' => 'неправильный токен']);
        }

        try {
            $refreshToken = JWTAuth::refresh($token);
        } catch (JWTException $exception) {
            return response()->json(['error' => 'ошибка']);
        }

        return response([
            'token_type' => 'Bearer',
            'access_token' => $refreshToken,
        ]);
    }

    public function deleteToken()
    {
        $user = JWTAuth::parseToken()->authenticate();

        if(!$user){
            // нет юзера
        }

        $user->delete();

    }

    public function index(Request $request){


        return User::all();
    }
}
