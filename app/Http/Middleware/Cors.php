<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{

    public function handle($request, Closure $next)
    {

        // разрешение CORS на определенные урлы
        $domains = ["http://localhost:8080"];
/*        if (isset($request->server()["HTTP_ORIGIN"])) {
            $origin = $request->server()["HTTP_ORIGIN"];
            if (in_array($origin, $domains)) {
                return $next($request)
                    ->header('Access-Control-Allow-Origin', $origin)
                    ->header('Access-Control-Allow-Headers' ,'Origin, Content-Type, Authorization');
            }
        }*/



        // разрешение CORS всем


        return $next($request)->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Headers' ,'Origin, Content-Type, Authorization');
    }
}
