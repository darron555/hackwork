<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class ApiUserAuth
{

    public function handle($request, Closure $next)
    {
        try {
            if (!$user = JWTAuth::parseToken()->toUser()) {
                return response()->json(['error' => 'Пользователь не найден'], 404);
            }
        } catch (JWTException $exception) {
            return response()->json(['error' => 'Ошибка']);
        }

        return $next($request);
    }


}
